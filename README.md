# README #

This is the PCB design for my second quadcopter project.
Altium Designer software is required to open those files.

**The pictures below illustrate the mentioned PCB design.**
![1.PNG](https://bitbucket.org/repo/z4zRAo/images/2519362671-1.PNG)

![2.PNG](https://bitbucket.org/repo/z4zRAo/images/3665513965-2.PNG)

![3.PNG](https://bitbucket.org/repo/z4zRAo/images/3108172442-3.PNG)

**Testing the new board.**
![Capture.PNG](https://bitbucket.org/repo/z4zRAo/images/138094156-Capture.PNG)